#pragma once

#include <Arduino.h>

#include "config.hpp"
#include "pad.hpp"

namespace SNESPad2USB {

struct Range {
  constexpr Range(const uint16_t beg, const uint16_t end) : beg(beg), end(end), size(end - beg) {}
  const uint16_t beg;
  const uint16_t end;
  const uint16_t size;
};

class MemoryMgr256k {
 public:
  MemoryMgr256k(const uint8_t);
  void LoadConfig(Config*);
  void SaveConfig(const Config&);
  void SaveCurrentSetting(const uint8_t);
  void LoadSetting(ParameterSet*, const uint8_t);
  void SaveSetting(const ParameterSet&, const uint8_t);
  void GetSettingName(char[17], const uint8_t);
  uint8_t GetSettingIndexLen() { return kSettingIndexLen; }
  
 private:
  static constexpr Range kMemRange{0x0000, 0x8000};
  static constexpr Range kConfigRange{0x0000, 0x0080};
  static constexpr Range kSettingRange{0x0080, 0x2000};
  static constexpr Range kReservedRange{0x2000, 0x8000};
  static constexpr uint8_t kSettingIndexLen{kSettingRange.size / sizeof(ParameterSet)};
  const uint8_t address_;
  
};

}
