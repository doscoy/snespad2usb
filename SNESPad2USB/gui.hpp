#pragma once

#include <Arduino.h>
#include <Adafruit_SSD1306.h>

#include "config.hpp"
#include "pad.hpp"
#include "usb_mgr.hpp"
#include "mem_mgr.hpp"

namespace SNESPad2USB {

class GUI128x64 {
 public:
  GUI128x64(Config*, SNESPad*, USBReportMgr*, MemoryMgr256k*);
  void Init(uint8_t);
  void PrintString(const String&);
  void OffDisplay();
  void Draw();
  bool GetEnable() const { return enable_; }
  void SetEnable(const bool enable) { enable_ = enable; }

 private:
  static constexpr uint8_t kWidth{128};
  static constexpr uint8_t kHeight{64};
  void Separator() { display_.println("--------------------"); }
  void DrawMenu();
  void DrawSetting();
  void DrawLoadList();
  void DrawSaveList();
  void DrawNameEditor();
  void DrawButtonEditor();
  void DrawKeyboardParam();
  void DrawGamepadParam();
  void ChoseEditParam();
  void DrawInstSelector();
  void DrawParamLenSelector();
  void DrawKeycodeSelector();
  void DrawGamepadButtonSelector();
  void Draw8bitIntSelector();
  void DrawConfig();
  void Restart();
  void InitAll();
  void DrawInfo();
  void Exit();
  Adafruit_SSD1306 display_;
  Config& config_;
  SNESPad& pad_;
  USBReportMgr& usb_;
  MemoryMgr256k& mem_;
  uint8_t scene_{0};
  uint8_t cursor_[8]{0};
  bool enable_{false};
  unsigned long start_time_micro_{0};
  unsigned long end_time_micro_{0};
};

}
