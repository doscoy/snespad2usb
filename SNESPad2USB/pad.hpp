#pragma once

#include <Arduino.h>

#include "usb_mgr.hpp"

namespace SNESPad2USB {

enum : uint8_t {
  INST_NONE = 0,
  INST_FLIP,
  INST_KEYBOARD,
  INST_GAMEPAD,
  INST_MAX
};

enum {
  SNESPAD_B = 0,
  SNESPAD_Y,
  SNESPAD_SELECT,
  SNESPAD_START,
  SNESPAD_UP,
  SNESPAD_DOWN,
  SNESPAD_LEFT,
  SNESPAD_RIGHT,
  SNESPAD_A,
  SNESPAD_X,
  SNESPAD_L,
  SNESPAD_R,
  SNESPAD_MAX
};

enum : uint8_t {
  GAMEPAD_BUTTON1 = 0,
  GAMEPAD_BUTTON2,
  GAMEPAD_BUTTON3,
  GAMEPAD_BUTTON4,
  GAMEPAD_BUTTON5,
  GAMEPAD_BUTTON6,
  GAMEPAD_BUTTON7,
  GAMEPAD_BUTTON8,
  GAMEPAD_BUTTON9,
  GAMEPAD_BUTTON10,
  GAMEPAD_BUTTON11,
  GAMEPAD_BUTTON12,
  GAMEPAD_BUTTON13,
  GAMEPAD_BUTTON14,
  GAMEPAD_BUTTON15,
  GAMEPAD_BUTTON16,
  GAMEPAD_AXISX,
  GAMEPAD_AXISY,
  GAMEPAD_AXISZ,
  GAMEPAD_ROTATIONZ,
  GAMEPAD_MAX
};

class Button {
 public:
  Button(){};
  void Update(const bool state) {
    up_ = press_ && !state;
    down_ = !press_ && state;
    press_ = state;
  }
  bool Up() const { return up_; }
  bool Down() const { return down_; }
  bool Press() const { return press_; }
  bool Release() const { return !press_; }
  
 private:
  bool press_{false};
  bool up_{false};
  bool down_{false};
};

union Instruction {
  struct Map {
    uint8_t param_len : 2;
    uint8_t content : 6;
  };
  Map map;
  uint8_t raw;
};

struct ButtonParameter {
  Instruction inst;
  byte item[3];
};

struct ParameterSet {
  char name[17];
  byte reserved[15];
  ButtonParameter params[SNESPAD_MAX][2];
};

class SNESPad {
 public:
  SNESPad(uint8_t, uint8_t, uint8_t);
  void Init();
  void UpdateButton();
  void UpdateReport();
  void SetDefaultParams();
  bool Up(const int button) const { return buttons_[button].Up(); }
  bool Down(const int button) const { return buttons_[button].Down(); }
  bool Press(const int button) const { return buttons_[button].Press(); }
  bool Release(const int button) const { return buttons_[button].Release(); }
  auto GetSelectPressTime() const { return buttons_[SNESPAD_SELECT].Press() ? millis() - select_press_time_ : 0; }
  void ResetSelectPressTime() { select_press_time_ = millis(); }
  char* GetName() { return setting_.name; }
  uint8_t GetNameLen() const { return sizeof(setting_.name) - 1; }
  bool GetPressFlag() const { return press_flag_; }
  bool GetFlipFlag() const { return flip_flag_; }
  ParameterSet* GetParameterSet() { return &setting_; }
  ButtonParameter* GetParameter(const int button) { return setting_.params[button]; }
  KeyboardReport& GetKeyboardReport() { return keyboard_report_; }
  GamepadReport& GetGamepadReport() { return gamepad_report_; }
  
 private:
  static void WriteKeyboardReport(const Button&, const ButtonParameter&, KeyboardReport*);
  static void WriteGamePadReport(const Button&, const ButtonParameter&, GamepadReport*);
  const uint8_t data_pin_;
  const uint8_t load_pin_;
  const uint8_t clock_pin_;
  Button buttons_[SNESPAD_MAX];
  ParameterSet setting_;
  bool flip_flag_{false};
  bool press_flag_{false};
  KeyboardReport keyboard_report_;
  GamepadReport gamepad_report_;
  unsigned long select_press_time_{0};
};

}
