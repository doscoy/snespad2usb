#include "gui.hpp"

#include <Adafruit_SleepyDog.h>

namespace SNESPad2USB {

GUI128x64::GUI128x64(Config* config, SNESPad* pad, USBReportMgr* usb, MemoryMgr256k* mem)
    : display_(kWidth, kHeight), config_(*config), pad_(*pad), usb_(*usb), mem_(*mem) {}

void GUI128x64::Init(uint8_t oled_address) {
  display_.begin(SSD1306_SWITCHCAPVCC, oled_address, true, false);
  display_.ssd1306_command(0xC0);
  display_.ssd1306_command(0xA0);
  display_.clearDisplay();
}

void GUI128x64::PrintString(const String& str) {
  display_.clearDisplay();
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(str);
  display_.display();
}

void GUI128x64::OffDisplay() {
  display_.clearDisplay();
  display_.display();
}

namespace {

template <class T>
void SubCursor(T* target, const T sub, const T min_num) {
  const T tmp(*target);
  *target -= sub;
  if (*target < min_num || tmp < *target) *target = min_num;
}

template <class T>
void SubCursor(T* target, const T sub, const T min_num, const T loop_num) {
  const T tmp(*target);
  *target -= sub;
  if (*target < min_num || tmp < *target) *target = loop_num;
}

template <class T>
void AddCursor(T* target, const T add, const T max_num) {
  const T tmp(*target);
  *target += add;
  if (max_num < *target || *target < tmp) *target = max_num;
}

template <class T>
void AddCursor(T* target, const T add, const T max_num, const T loop_num) {
  const T tmp(*target);
  *target += add;
  if (max_num < *target || *target < tmp) *target = loop_num;
}

void ScrollRange(uint8_t* scroll_cursor, const uint8_t item_cursor, const uint8_t row) {
  const uint8_t tmp{static_cast<uint8_t>(*scroll_cursor + row - 1)};
  if ((*scroll_cursor <= item_cursor) && (item_cursor <= tmp)) {
    return;
  } else if (tmp < item_cursor) {
    *scroll_cursor = item_cursor - row + 1;
    return;
  } else if (item_cursor < *scroll_cursor) {
    *scroll_cursor = item_cursor;
    return;
  }
}
}

namespace {
enum : uint8_t {
  SCENE_MENU = 0,
  SCENE_LOAD,
  SCENE_SAVE,
  SCENE_SETTING,
  SCENE_NAMEEDIT,
  SCENE_BUTTONEDIT,
  SCENE_INSTSELECT,
  SCENE_PARAMLENSELECT,
  SCENE_KEYCODESELECT,
  SCENE_PADBUTTONSELECT,
  SCENE_8BITINTSELECT,
  SCENE_CONFIG,
  SCENE_INFO
};
}

void GUI128x64::Draw() {
  start_time_micro_ = micros();
  display_.clearDisplay();
  switch(scene_) {
    case SCENE_MENU:
      DrawMenu();
      break;
    case SCENE_LOAD:
      DrawLoadList();
      break;
    case SCENE_SAVE:
      DrawSaveList();
      break;
    case SCENE_SETTING:
      DrawSetting();
      break;
    case SCENE_NAMEEDIT:
      DrawNameEditor();
      break;
    case SCENE_BUTTONEDIT:
      DrawButtonEditor();
      break;
    case SCENE_INSTSELECT:
      DrawInstSelector();
      break;
    case SCENE_PARAMLENSELECT:
      DrawParamLenSelector();
      break;
    case SCENE_KEYCODESELECT:
      DrawKeycodeSelector();
      break;
    case SCENE_PADBUTTONSELECT:
      DrawGamepadButtonSelector();
      break;
    case SCENE_8BITINTSELECT:
      Draw8bitIntSelector();
      break;
    case SCENE_CONFIG:
      DrawConfig();
      break;
    case SCENE_INFO:
      DrawInfo();
      break;
    default:
      display_.println("unknow scene");
      break;
  }
  display_.display();
  delay(10);
  GamepadReport dummy_g{};
  KeyboardReport dummy_k{};
  usb_.SendGamepadReport(dummy_g);
  usb_.SendKeyboardReport(dummy_k);
  end_time_micro_ = micros();
}

void GUI128x64::Exit() {
  scene_ = SCENE_MENU;
  memset(cursor_, 0, sizeof(cursor_));
  enable_ = false;
  display_.clearDisplay();
}

namespace {
constexpr uint8_t kMenuItemLen{6};
constexpr char kMenuItems[kMenuItemLen][8]{
  "Load",
  "Save",
  "Setting",
  "-------",
  "Config",
  "Info"
};
}

void GUI128x64::DrawMenu() {
  uint8_t* item_cursor{&cursor_[0]};
  
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(pad_.GetName());
  Separator();
  
  for (uint8_t i = 0; i < kMenuItemLen; ++i) {
    if (i == *item_cursor) {
      display_.setTextColor(BLACK, WHITE);
      display_.println(kMenuItems[i]);
      display_.setTextColor(WHITE);
    } else {
      display_.println(kMenuItems[i]);
    }
  }

  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0, kMenuItemLen - 1);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, kMenuItemLen - 1, 0);
  if (pad_.Down(SNESPAD_B)) {
    switch (*item_cursor) {
      case 0:
        cursor_[1] = config_.current_setting;
        scene_ = SCENE_LOAD;
        break;
      case 1:
        cursor_[1] = config_.current_setting;
        scene_ = SCENE_SAVE;
        break;
      case 2:
        scene_ = SCENE_SETTING;
        break;
      case 3:
        break;
      case 4:
        scene_ = SCENE_CONFIG;
        break;
      case 5:
        scene_ = SCENE_INFO;
        break;
      default:
        break;
    }
    return;
  }
  if (pad_.Down(SNESPAD_Y)) Exit();
}

void GUI128x64::DrawLoadList() {
  uint8_t* item_cursor{&cursor_[1]};
  uint8_t* scroll_cursor{&cursor_[2]};
  
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(pad_.GetName());
  Separator();

  char name_buffer[17];
  ScrollRange(scroll_cursor, *item_cursor, 6);
  for (uint8_t i = *scroll_cursor; i < *scroll_cursor + 6; ++i) {
    if (i == config_.current_setting) {
      display_.setTextColor(BLACK, WHITE);
      if (i < 10) display_.print('0');
      display_.print(i);
      display_.setTextColor(WHITE);
    } else {
      if (i < 10) display_.print('0');
      display_.print(i);
    }
    display_.print('_');
    mem_.GetSettingName(name_buffer, i);
    if (i == *item_cursor) {
      display_.setTextColor(BLACK, WHITE);
      display_.println(name_buffer);
      display_.setTextColor(WHITE);
    } else {
      display_.println(name_buffer);
    }
  }

  const uint8_t index_len{mem_.GetSettingIndexLen()};
  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0, index_len - 1);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, index_len - 1, 0);
  if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint8_t>(item_cursor, 3, 0, index_len - 1);
  if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint8_t>(item_cursor, 3, index_len - 1, 0);
  if (pad_.Down(SNESPAD_B)) {
    config_.current_setting = *item_cursor;
    mem_.LoadSetting(pad_.GetParameterSet(), *item_cursor);
    mem_.SaveCurrentSetting(*item_cursor);
    *scroll_cursor = 0;
    *item_cursor = 0;
    scene_ = SCENE_MENU;
    return;
  }
  if (pad_.Down(SNESPAD_Y)) {
    *scroll_cursor = 0;
    *item_cursor = 0;
    scene_ = SCENE_MENU;
    return;
  }
}

void GUI128x64::DrawSaveList() {
  uint8_t* item_cursor{&cursor_[1]};
  uint8_t* scroll_cursor{&cursor_[2]};
  
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(pad_.GetName());
  Separator();

  char name_buffer[17];
  ScrollRange(scroll_cursor, *item_cursor, 6);
  for (uint8_t i = *scroll_cursor; i < *scroll_cursor + 6; ++i) {
    if (i == config_.current_setting) {
      display_.setTextColor(BLACK, WHITE);
      if (i < 10) display_.print('0');
      display_.print(i);
      display_.setTextColor(WHITE);
    } else {
      if (i < 10) display_.print('0');
      display_.print(i);
    }
    display_.print('_');
    mem_.GetSettingName(name_buffer, i);
    if (i == *item_cursor) {
      display_.setTextColor(BLACK, WHITE);
      display_.println(name_buffer);
      display_.setTextColor(WHITE);
    } else {
      display_.println(name_buffer);
    }
  }

  const uint8_t index_len{mem_.GetSettingIndexLen()};
  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0, index_len - 1);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, index_len - 1, 0);
  if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint8_t>(item_cursor, 3, 0, index_len - 1);
  if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint8_t>(item_cursor, 3, index_len - 1, 0);
  if (pad_.Down(SNESPAD_B)) {
    config_.current_setting = *item_cursor;
    mem_.SaveSetting(*pad_.GetParameterSet(), *item_cursor);
    mem_.SaveCurrentSetting(*item_cursor);
    *scroll_cursor = 0;
    *item_cursor = 0;
    scene_ = SCENE_MENU;
    return;
  }
  if (pad_.Down(SNESPAD_Y)) {
    *scroll_cursor = 0;
    *item_cursor = 0;
    scene_ = SCENE_MENU;
    return;
  }
}

namespace {
constexpr uint8_t kSettingItemLen{SNESPAD_MAX + 1};
constexpr char kSettingItems[kSettingItemLen][8]{
  "B",
  "Y",
  "SELECT",
  "START",
  "UP",
  "DOWN",
  "LEFT",
  "RIGHT",
  "A",
  "X",
  "L",
  "R",
  "Name"
};
}

void GUI128x64::DrawSetting() {
  uint8_t* scroll_cursor{&cursor_[1]};
  uint8_t* item_cursor{&cursor_[2]};
  
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(pad_.GetName());
  Separator();
  
  ScrollRange(scroll_cursor, *item_cursor, 6);
  for (uint8_t i = *scroll_cursor; i < *scroll_cursor + 6; ++i) {
    if (i == *item_cursor) {
      display_.setTextColor(BLACK, WHITE);
      display_.println(kSettingItems[i]);
      display_.setTextColor(WHITE);
    } else {
      display_.println(kSettingItems[i]);
    }
  }

  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0, kSettingItemLen - 1);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, kSettingItemLen - 1, 0);
  if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint8_t>(item_cursor, 3, 0, kSettingItemLen - 1);
  if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint8_t>(item_cursor, 3, kSettingItemLen - 1, 0);
  if (pad_.Down(SNESPAD_B)) {
    if (*item_cursor < SNESPAD_MAX) {
      scene_ = SCENE_BUTTONEDIT;
      return;
    } else if (*item_cursor == SNESPAD_MAX) {
      scene_ = SCENE_NAMEEDIT;
      return;
    }
  }
  if (pad_.Down(SNESPAD_Y)) {
    *scroll_cursor = 0;
    *item_cursor = 0;
    scene_ = SCENE_MENU;
    return;
  }
}

namespace {
constexpr uint8_t kInputCharStart{32};
constexpr uint8_t kInputCharEnd{126};
}

void GUI128x64::DrawNameEditor() {
  uint8_t& output_cursor{cursor_[3]};
  uint8_t& input_cursor{cursor_[4]};

  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  uint8_t upper_position{static_cast<uint8_t>(pad_.GetNameLen() - 1)};
  for (uint8_t i = 0; i < pad_.GetNameLen(); ++i) {
    if (pad_.GetName()[i] == 0) {
      upper_position = i;
      break;
    }
    if (i == output_cursor) {
      display_.setTextColor(BLACK, WHITE);
      display_.print(pad_.GetName()[i]);
      display_.setTextColor(WHITE);
    } else {
      display_.print(pad_.GetName()[i]);
    }
  }
  display_.print('\n');
  Separator();

  if (input_cursor < kInputCharStart) input_cursor = kInputCharStart;
  if (kInputCharEnd < input_cursor) input_cursor = kInputCharEnd;
  for (uint8_t i = kInputCharStart; i <= kInputCharEnd; ++i) {
    if (i == input_cursor) {
      display_.setTextColor(BLACK, WHITE);
      display_.print(static_cast<char>(i));
      display_.setTextColor(WHITE);
    } else {
      display_.print(static_cast<char>(i));
    }
  }
  display_.print('\n');
  
  if (pad_.Down(SNESPAD_L)) SubCursor<uint8_t>(&output_cursor, 1, 0);
  if (pad_.Down(SNESPAD_R)) AddCursor<uint8_t>(&output_cursor, 1, upper_position);
  if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint8_t>(&input_cursor, 1, kInputCharStart);
  if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint8_t>(&input_cursor, 1, kInputCharEnd);
  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(&input_cursor, 21, kInputCharStart);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(&input_cursor, 21, kInputCharEnd);
  if (pad_.Down(SNESPAD_B)) {
    pad_.GetName()[output_cursor] = input_cursor;
    AddCursor<uint8_t>(&output_cursor, 1, pad_.GetNameLen() - 1);
  }
  if (pad_.Down(SNESPAD_Y)) {
    for (uint8_t i = output_cursor; i <= pad_.GetNameLen() - 1; ++i) {
      pad_.GetName()[i] = pad_.GetName()[i + 1];
    }
    pad_.GetName()[pad_.GetNameLen() - 1] = 0;
  }
  if (upper_position != 0) {
    display_.print("push START to return");
    if (pad_.Down(SNESPAD_START)) {
      output_cursor = 0;
      input_cursor = 0;
      scene_ = SCENE_SETTING;
      return;
    }
  } else {
    display_.print("input name");
  }
}

namespace {
constexpr char kFlipDesc[2][8]{
  "Front",
  "Back"
};
constexpr char kInstName[INST_MAX][16]{
  "None",
  "Flip",
  "Keyboard",
  "Gamepad"
};
}

void GUI128x64::DrawButtonEditor() {
  const uint8_t target_cursor{cursor_[2]};
  uint8_t* flip_cursor{&cursor_[3]};
  uint8_t* item_cursor{&cursor_[4]};
  ButtonParameter* target_param{pad_.GetParameter(target_cursor)};

  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(kSettingItems[target_cursor]);
  Separator();

  if (*flip_cursor > 1) *flip_cursor = 1;
  display_.println(kFlipDesc[*flip_cursor]);

  const uint8_t inst{target_param[*flip_cursor].inst.map.content};
  if (*item_cursor == 0) {
    display_.setTextColor(BLACK, WHITE);
    display_.println(kInstName[inst]);
    display_.setTextColor(WHITE);
  } else {
    display_.println(kInstName[inst]);
  }

  switch (inst) {
    case INST_KEYBOARD:
      DrawKeyboardParam();
      break;
    case INST_GAMEPAD:
      DrawGamepadParam();
      break;
    default:
      break;
  }

  if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint8_t>(flip_cursor, 1, 0, 1);
  if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint8_t>(flip_cursor, 1, 1, 0);
  if (pad_.Down(SNESPAD_B)) {
    ChoseEditParam();
    return;
  }
  if (pad_.Down(SNESPAD_Y)) {
    *item_cursor = 0;
    scene_ = SCENE_SETTING;
    return;
  }
}

namespace {
constexpr uint8_t kKeycodeLen{0xE8};
constexpr char kKeyboardParam[kKeycodeLen][20] {
  "NONE",          "----",        "----",              "----",              "A",                  "B",               "C",                 "D",                  "E",            "F",               "G",                "H",        "I",        "J",        "K",         "L",
  "M",             "N",           "O",                 "P",                 "Q",                  "R",               "S",                 "T",                  "U",            "V",               "W",                "X",        "Y",        "Z",        "1",         "2",
  "3",             "4",           "5",                 "6",                 "7",                  "8",               "9",                 "0",                  "ENTER",        "ESCAPE",          "BACKSPACE",        "TAB",      "SPACE",    "MINUS",    "EQUAL",     "BRACKET_LEFT",
  "BRACKET_RIGHT", "BACKSLASH",   "EUROPE_1",          "SEMICOLON",         "APOSTROPHE",         "GRAVE",           "COMMA",             "PERIOD",             "SLASH",        "CAPS_LOCK",       "F1",               "F2",       "F3",       "F4",       "F5",        "F6",
  "F7",            "F8",          "F9",                "F10",               "F11",                "F12",             "PRINT_SCREEN",      "SCROLL_LOCK",        "PAUSE",        "INSERT",          "HOME",             "PAGE_UP",  "DELETE",   "END",      "PAGE_DOWN", "ARROW_RIGHT",
  "ARROW_LEFT",    "ARROW_DOWN",  "ARROW_UP",          "NUM_LOCK",          "KEYPAD_DIVIDE",      "KEYPAD_MULTIPLY", "KEYPAD_SUBTRACT",   "KEYPAD_ADD",         "KEYPAD_ENTER", "KEYPAD_1",        "KEYPAD_2",         "KEYPAD_3", "KEYPAD_4", "KEYPAD_5", "KEYPAD_6",  "KEYPAD_7",
  "KEYPAD_8",      "KEYPAD_9",    "KEYPAD_0",          "KEYPAD_DECIMAL",    "EUROPE_2",           "APPLICATION ",    "POWER",             "KEYPAD_EQUAL",       "F13",          "F14",             "F15",              "F16",      "F17",      "F18",      "F19",       "F20",
  "F21",           "F22",         "F23",               "F24",               "EXECUTE",            "HELP",            "MENU",              "SELECT",             "STOP",         "AGAIN",           "UNDO",             "CUT",      "COPY",     "PASTE",    "FIND",      "MUTE",
  "VOLUME_UP",     "VOLUME_DOWN", "LOCKING_CAPS_LOCK", "LOCKING_NUM_LOCK",  "LOCKING_SCRLL_LOCK", "KEYPAD_COMMA",    "KEYPAD_EQUAL_SIGN", "KANJI1",             "KANJI2",       "KANJI3",          "KANJI4",           "KANJI5",   "KANJI6",   "KANJI7",   "KANJI8",    "KANJI9",
  "LANG1",         "LANG2",       "LANG3",             "LANG4",             "LANG5",              "LANG6",           "LANG7",             "LANG8",              "LANG9",        "ALTERNATE_ERASE", "SYSREQ_ATTENTION", "CANCEL",   "CLEAR",    "PRIOR",    "RETURN",    "SEPARATOR",
  "OUT",           "OPER",        "CLEAR_AGAIN",       "CRSEL_PROPS",       "EXSEL",              "-----",           "-----",             "-----",              "-----",        "-----",           "-----",            "-----",    "-----",    "-----",    "-----",     "-----",
  "-----",         "-----",       "-----",             "-----",             "-----",              "-----",           "-----",             "-----",              "-----",        "-----",           "-----",            "-----",    "-----",    "-----",    "-----",     "-----",
  "-----",         "-----",       "-----",             "-----",             "-----",              "-----",           "-----",             "-----",              "-----",        "-----",           "-----",            "-----",    "-----",    "-----",    "-----",     "-----",
  "-----",         "-----",       "-----",             "-----",             "-----",              "-----",           "-----",             "-----",              "-----",        "-----",           "-----",            "-----",    "-----",    "-----",    "-----",     "-----",
  "CONTROL_LEFT",  "SHIFT_LEFT",  "ALT(OPT)_LEFT",     "WIN(COMMAND)_LEFT", "CONTROL_RIGHT",      "SHIFT_RIGHT",     "ALT(OPT)_RIGHT",    "WIN(COMMAND)_RIGHT"
};
void PrintByteHexHelper(Adafruit_SSD1306* display, uint8_t target) {
  if (target < 0x10) {
    display->print(0);
    display->print(target, HEX);
  } else {
    display->print(target, HEX);
  }
}
}

void GUI128x64::DrawKeyboardParam() {
  const uint8_t target_cursor{cursor_[2]};
  const uint8_t flip_cursor{cursor_[3]};
  uint8_t* item_cursor{&cursor_[4]};
  const ButtonParameter& target_param{pad_.GetParameter(target_cursor)[flip_cursor]};
  const uint8_t param_len{target_param.inst.map.param_len};

  if (*item_cursor == 1) {
    display_.setTextColor(BLACK, WHITE);
    display_.println(param_len);
    display_.setTextColor(WHITE);
  } else {
    display_.println(param_len);
  }

  const uint8_t* const item{target_param.item};
  for (uint8_t i = 0; i < param_len; ++i) {
    if (*item_cursor == i + 2) {
      display_.setTextColor(BLACK, WHITE);
      PrintByteHexHelper(&display_, item[i]);
      display_.print('_');
      display_.println(kKeyboardParam[item[i]]);
      display_.setTextColor(WHITE);
    } else {
      PrintByteHexHelper(&display_, item[i]);
      display_.print('_');
      display_.println(kKeyboardParam[item[i]]);
    }
  }

  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, param_len + 1);
}

namespace {
constexpr char kGamepadParam[GAMEPAD_MAX][16] {
  "Button 1", "Button 2",  "Button 3",  "Button 4",  "Button 5",  "Button 6",  "Button 7",  "Button 8",
  "Button 9", "Button 10", "Button 11", "Button 12", "Button 13", "Button 14", "Button 15", "Button 16",
  "Axis X",   "Axis Y",    "Axis Z",    "Rotation Z"
};
}

void GUI128x64::DrawGamepadParam() {
  const uint8_t target_cursor{cursor_[2]};
  const uint8_t flip_cursor{cursor_[3]};
  uint8_t* item_cursor{&cursor_[4]};
  const ButtonParameter& target_param{pad_.GetParameter(target_cursor)[flip_cursor]};
  const uint8_t param_len{target_param.inst.map.param_len};

  const uint8_t item0{target_param.item[0]};
  if (*item_cursor == 1) {
    display_.setTextColor(BLACK, WHITE);
    display_.println(kGamepadParam[item0]);
    display_.setTextColor(WHITE);
  } else {
    display_.println(kGamepadParam[item0]);
  }

  if (target_param.item[0] >= 16) {
    const uint8_t item1{target_param.item[1]};
    if (*item_cursor == 2) {
      display_.setTextColor(BLACK, WHITE);
      display_.println(static_cast<int8_t>(item1 - 128));
      display_.setTextColor(WHITE);
    } else {
      display_.println(static_cast<int8_t>(item1 - 128));
    }
  }

  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, param_len);
}

void GUI128x64::ChoseEditParam() {
  const uint8_t target_cursor{cursor_[2]};
  const uint8_t flip_cursor{cursor_[3]};
  const uint8_t item_cursor{cursor_[4]};
  const ButtonParameter& target_param{pad_.GetParameter(target_cursor)[flip_cursor]};
  
  if (item_cursor == 0) {
    cursor_[5] = target_param.inst.map.content;
    scene_ = SCENE_INSTSELECT;
    return;
  } else if (item_cursor == 1) {
    switch (target_param.inst.map.content) {
      case INST_KEYBOARD:
        cursor_[5] = target_param.inst.map.param_len;
        scene_ = SCENE_PARAMLENSELECT;
        return;
      case INST_GAMEPAD:
        cursor_[5] = target_param.item[0];
        scene_ = SCENE_PADBUTTONSELECT;
        return;
      default:
        return;
    }
  } else if (item_cursor == 2) {
    switch (target_param.inst.map.content) {
      case INST_KEYBOARD:
        cursor_[5] = 0;
        cursor_[6] = target_param.item[0];
        scene_ = SCENE_KEYCODESELECT;
        return;
      case INST_GAMEPAD:
        cursor_[5] = target_param.item[1];
        scene_ = SCENE_8BITINTSELECT;
        return;
      default:
        return;
    }
  } else if (item_cursor == 3) {
    cursor_[5] = 1;
    cursor_[6] = target_param.item[1];
    scene_ = SCENE_KEYCODESELECT;
    return;
  } else if (item_cursor == 4) {
    cursor_[5] = 2;
    cursor_[6] = target_param.item[2];
    scene_ = SCENE_KEYCODESELECT;
    return;
  }
}

namespace {
void FlipTreatHelper(ButtonParameter*, const uint8_t);
void InstInitHelper(ButtonParameter* param, const uint8_t flip) {
  switch (param[flip].inst.map.content) {
    case INST_NONE:
      param[flip].inst.map.param_len = 0;
      FlipTreatHelper(param, flip);
      break;
    case INST_FLIP:
      param[flip].inst.map.param_len = 0;
      param[(flip + 1) % 2].inst.map.content = INST_FLIP;
      param[(flip + 1) % 2].inst.map.param_len = 0;
      param[(flip + 1) % 2].item[0] = 0;
      param[(flip + 1) % 2].item[1] = 0;
      param[(flip + 1) % 2].item[2] = 0;
      break;
    case INST_KEYBOARD:
      param[flip].inst.map.param_len = 1;
      FlipTreatHelper(param, flip);
      break;
    case INST_GAMEPAD:
      param[flip].inst.map.param_len = 1;
      FlipTreatHelper(param, flip);
      break;
    default:
      param[flip].inst.map.content = INST_NONE;
      param[flip].inst.map.param_len = 0;
      FlipTreatHelper(param, flip);
      break;
  }
  param[flip].item[0] = 0;
  param[flip].item[1] = 0;
  param[flip].item[2] = 0;
}
void FlipTreatHelper(ButtonParameter* param, const uint8_t flip) {
  const uint8_t other{static_cast<uint8_t>((flip + 1) % 2)};
  if (param[other].inst.map.content == INST_FLIP) {
    param[other].inst.map.content = param[flip].inst.map.content;
    InstInitHelper(param, other);
  }
}
}

void GUI128x64::DrawInstSelector() {
  const uint8_t target_cursor{cursor_[2]};
  const uint8_t flip_cursor{cursor_[3]};
  uint8_t* item_cursor{&cursor_[5]};
  ButtonParameter* target_param{pad_.GetParameter(target_cursor)};

  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(kSettingItems[target_cursor]);
  Separator();

  for (uint8_t i = 0; i < INST_MAX; ++i) {
    if (*item_cursor == i) {
      display_.setTextColor(BLACK, WHITE);
      display_.println(kInstName[i]);
      display_.setTextColor(WHITE);
    } else {
      display_.println(kInstName[i]);
    }
  }

  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0, INST_MAX - 1);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, INST_MAX - 1, 0);
  if (pad_.Down(SNESPAD_B)) {
    if (target_param[flip_cursor].inst.map.content != *item_cursor) {
      target_param[flip_cursor].inst.map.content = *item_cursor;
      InstInitHelper(target_param, flip_cursor);
    }
    *item_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
  if (pad_.Down(SNESPAD_Y)) {
    *item_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
}

void GUI128x64::DrawParamLenSelector() {
  const uint8_t target_cursor{cursor_[2]};
  const uint8_t flip_cursor{cursor_[3]};
  uint8_t* item_cursor{&cursor_[5]};
  ButtonParameter* target_param{&pad_.GetParameter(target_cursor)[flip_cursor]};

  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(kSettingItems[target_cursor]);
  Separator();

  const uint8_t item_len{sizeof(target_param->item)};
  if (*item_cursor < 1) *item_cursor = 1;
  if (*item_cursor > item_len) *item_cursor = item_len;
  for (uint8_t i = 1; i <= item_len; ++i) {
    if (*item_cursor == i) {
      display_.setTextColor(BLACK, WHITE);
      display_.println(i);
      display_.setTextColor(WHITE);
    } else {
      display_.println(i);
    }
  }

  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 1);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, item_len);
  if (pad_.Down(SNESPAD_B)) {
    for (uint8_t i = *item_cursor; i <= item_len; ++i) {
      target_param->item[i] = 0;
    }
    target_param->inst.map.param_len = *item_cursor;
    *item_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
  if (pad_.Down(SNESPAD_Y)) {
    *item_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
}

void GUI128x64::DrawKeycodeSelector() {
  const uint8_t target_cursor{cursor_[2]};
  const uint8_t flip_cursor{cursor_[3]};
  uint8_t* keynum_cursor{&cursor_[5]};
  uint8_t* item_cursor{&cursor_[6]};
  uint8_t* scroll_cursor{&cursor_[7]};
  ButtonParameter* target_param{&pad_.GetParameter(target_cursor)[flip_cursor]};

  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(kSettingItems[target_cursor]);
  Separator();

  ScrollRange(scroll_cursor, *item_cursor, 6);
  for (uint8_t i = *scroll_cursor; i < *scroll_cursor + 6; ++i) {
    if (*item_cursor == i) {
      display_.setTextColor(BLACK, WHITE);
      PrintByteHexHelper(&display_, i);
      display_.print('_');
      display_.println(kKeyboardParam[i]);
      display_.setTextColor(WHITE);
    } else {
      PrintByteHexHelper(&display_, i);
      display_.print('_');
      display_.println(kKeyboardParam[i]);
    }
  }

  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0, kKeycodeLen - 1);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, kKeycodeLen - 1, 0);
  if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint8_t>(item_cursor, 6, 0, kKeycodeLen - 1);
  if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint8_t>(item_cursor, 6, kKeycodeLen - 1, 0);
  if (pad_.Down(SNESPAD_B)) {
    target_param->item[*keynum_cursor] = *item_cursor;
    *keynum_cursor = 0;
    *item_cursor = 0;
    *scroll_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
  if (pad_.Down(SNESPAD_Y)) {
    *keynum_cursor = 0;
    *item_cursor = 0;
    *scroll_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
}

void GUI128x64::DrawGamepadButtonSelector() {
  const uint8_t target_cursor{cursor_[2]};
  const uint8_t flip_cursor{cursor_[3]};
  uint8_t* item_cursor{&cursor_[5]};
  uint8_t* scroll_cursor{&cursor_[6]};
  ButtonParameter* target_param{&pad_.GetParameter(target_cursor)[flip_cursor]};

  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(kSettingItems[target_cursor]);
  Separator();

  ScrollRange(scroll_cursor, *item_cursor, 6);
  for (uint8_t i = *scroll_cursor; i < *scroll_cursor + 6; ++i) {
    if (*item_cursor == i) {
      display_.setTextColor(BLACK, WHITE);
      display_.println(kGamepadParam[i]);
      display_.setTextColor(WHITE);
    } else {
      display_.println(kGamepadParam[i]);
    }
  }

  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0, GAMEPAD_MAX - 1);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, GAMEPAD_MAX - 1, 0);
  if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint8_t>(item_cursor, 3, 0, GAMEPAD_MAX - 1);
  if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint8_t>(item_cursor, 3, GAMEPAD_MAX - 1, 0);
  if (pad_.Down(SNESPAD_B)) {
    target_param->inst.map.param_len = *item_cursor <= GAMEPAD_BUTTON16 ? 1 : 2;
    target_param->item[0] = *item_cursor;
    *scroll_cursor = 0;
    *item_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
  if (pad_.Down(SNESPAD_Y)) {
    *scroll_cursor = 0;
    *item_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
}

void GUI128x64::Draw8bitIntSelector() {
  const uint8_t target_cursor{cursor_[2]};
  const uint8_t flip_cursor{cursor_[3]};
  uint8_t* item_cursor{&cursor_[5]};
  ButtonParameter* target_param{&pad_.GetParameter(target_cursor)[flip_cursor]};

  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println(kSettingItems[target_cursor]);
  Separator();

  display_.setTextColor(BLACK, WHITE);
  display_.println(static_cast<int8_t>(*item_cursor - 128));
  display_.setTextColor(WHITE);
  display_.print('\n');
  display_.println("   +1   ");
  display_.println("-10  +10");
  display_.println("   -1   ");

  if (pad_.Down(SNESPAD_UP)) AddCursor<uint8_t>(item_cursor, 1, 255);
  if (pad_.Down(SNESPAD_DOWN)) SubCursor<uint8_t>(item_cursor, 1, 0);
  if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint8_t>(item_cursor, 10, 0);
  if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint8_t>(item_cursor, 10, 255);
  if (pad_.Down(SNESPAD_B)) {
    target_param->item[1] = *item_cursor;
    *item_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
  if (pad_.Down(SNESPAD_Y)) {
    *item_cursor = 0;
    scene_ = SCENE_BUTTONEDIT;
    return;
  }
}

void GUI128x64::DrawConfig() {
  uint8_t* item_cursor{&cursor_[1]};
  uint8_t item_counter{0};
  
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  
  display_.print("usb poll: ");
  if (*item_cursor == item_counter) {
    display_.setTextColor(BLACK, WHITE);
    display_.print(config_.usb_poll_interval);
    display_.setTextColor(WHITE);
    if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint8_t>(&config_.usb_poll_interval, 1, MYAPP_CONFIG_INTERVAL_MIN);
    if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint8_t>(&config_.usb_poll_interval, 1, MYAPP_CONFIG_INTERVAL_MAX);
  } else {
    display_.print(config_.usb_poll_interval);
  }
  display_.println("ms");
  display_.print("(Approx. ");
  display_.print(USBReportMgr::CalcFreq(config_.usb_poll_interval));
  display_.println("Hz)");
  ++item_counter;

  display_.print("menu call: ");
  if (*item_cursor == item_counter) {
    display_.setTextColor(BLACK, WHITE);
    display_.print(config_.menu_call);
    display_.setTextColor(WHITE);
    if (pad_.Down(SNESPAD_LEFT)) SubCursor<uint16_t>(&config_.menu_call, 100, MYAPP_CONFIG_CALL_MIN);
    if (pad_.Down(SNESPAD_RIGHT)) AddCursor<uint16_t>(&config_.menu_call, 100, MYAPP_CONFIG_CALL_MAX);
  } else {
    display_.print(config_.menu_call);
  }
  display_.println("ms");
  ++item_counter;
  ++item_counter;

  if (*item_cursor == item_counter) {
    display_.setTextColor(BLACK, WHITE);
    display_.println("Save and Restart");
    display_.setTextColor(WHITE);
    if (pad_.Down(SNESPAD_B)) {
      mem_.SaveConfig(config_);
      Restart();
    }
  } else {
    display_.println("Save and Restart");
  }
  ++item_counter;
  ++item_counter;

  if (*item_cursor == item_counter) {
    display_.setTextColor(BLACK, WHITE);
    display_.println("Restart");
    display_.setTextColor(WHITE);
    if (pad_.Down(SNESPAD_B)) {
      Restart();
    }
  } else {
    display_.println("Restart");
  }

  if (pad_.Down(SNESPAD_UP)) SubCursor<uint8_t>(item_cursor, 1, 0);
  if (pad_.Down(SNESPAD_DOWN)) AddCursor<uint8_t>(item_cursor, 1, item_counter);
  if (pad_.Down(SNESPAD_Y)) {
    *item_cursor = 0;
    scene_ = SCENE_MENU;
    return;
  }

  Separator();
  display_.println("SELECT START L R");
  display_.println("long press to init");

  if (pad_.Press(SNESPAD_SELECT) && pad_.Press(SNESPAD_START) && pad_.Press(SNESPAD_L) && pad_.Press(SNESPAD_R)) {
    if (pad_.GetSelectPressTime() > 5000) InitAll();
  }
}

void GUI128x64::Restart() {
  display_.clearDisplay();
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println("Shutdown...");
  display_.display();
  Watchdog.enable(1000);
  while (true) delay(1000);
}

void GUI128x64::InitAll() {
  display_.clearDisplay();
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println("Init config");
  display_.display();
  delay(500);
  config_.usb_poll_interval = MYAPP_CONFIG_INTERVAL_DEFAULT;
  config_.menu_call = MYAPP_CONFIG_CALL_DEFAULT;
  config_.current_setting = 0;
  mem_.SaveConfig(config_);

  pad_.SetDefaultParams();
  display_.clearDisplay();
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  for (int i = 0; i < mem_.GetSettingIndexLen(); ++i) {
    display_.clearDisplay();
    display_.setTextColor(WHITE);
    display_.setCursor(0, 0);
    display_.print("Init setting ");
    display_.println(i);
    display_.display();
    delay(100);
    mem_.SaveSetting(*pad_.GetParameterSet(), i);
  }

  display_.clearDisplay();
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);
  display_.println("Done");
  display_.display();
  delay(1000);
  display_.clearDisplay();

  cursor_[1] = 0;
  cursor_[2] = 0;
  cursor_[3] = 0;
  cursor_[4] = 0;
  cursor_[5] = 0;
  cursor_[6] = 0;
  cursor_[7] = 0;
  scene_ = SCENE_MENU;
}

namespace {

template <class T>
void PrintBinaryHelper(Adafruit_SSD1306* display, const T target, uint8_t len) {
  const unsigned int upper{sizeof(T) * 8};
  if (len > upper) {
    for (uint8_t i = 0; i < (len - upper); ++i) {
      display->print(0);
    }
    len = upper;
  }
  for (uint8_t bit = len - 1; bit < len; --bit) {
    display->print((target & (1 << bit)) >> bit);
  }
}

}

void GUI128x64::DrawInfo() {
  display_.setTextColor(WHITE);
  display_.setCursor(0, 0);

  if (usb_.Ready()) {
    display_.print("usb(o) ");
  } else {
    display_.print("usb(x) ");
  }
  display_.print((start_time_micro_ - end_time_micro_) / 1000.0f);
  display_.print("ms ");
  display_.print(pad_.GetSelectPressTime());
  display_.print("ms\n");
  
  for (uint8_t i = SNESPAD_MAX - 1; i < SNESPAD_MAX; --i) {
    if (pad_.Down(i)) {
      display_.print('d');
    } else if (pad_.Up(i)) {
      display_.print('u');
    } else if (pad_.Press(i)) {
      display_.print('p');
    } else {
      display_.print('-');
    }
  }
  display_.print('\n');

  if (pad_.GetFlipFlag()) {
    display_.println("back");
  } else {
    display_.println("front");
  }
  
  KeyboardReport k_repo{pad_.GetKeyboardReport()};
  display_.print(k_repo.count);
  display_.print(' ');
  PrintBinaryHelper(&display_, k_repo.mod, 8);
  display_.print('\n');
  for (uint8_t i = 0; i < k_repo.count; ++i) {
    PrintByteHexHelper(&display_, k_repo.keycode[i]);
    display_.print(' ');
  }
  display_.print('\n');
  
  GamepadReport g_repo{pad_.GetGamepadReport()};
  PrintBinaryHelper(&display_, g_repo.button, 12);
  display_.print("\nx");
  display_.print(static_cast<int8_t>(g_repo.axis_x - 128));
  display_.print("y");
  display_.print(static_cast<int8_t>(g_repo.axis_y - 128));
  display_.print("z");
  display_.print(static_cast<int8_t>(g_repo.axis_z - 128));
  display_.print("rz");
  display_.print(static_cast<int8_t>(g_repo.rot_z - 128));
  display_.print('\n');

  if (pad_.GetSelectPressTime() > config_.menu_call) {
    scene_ = SCENE_MENU;
    return;
  }
}

}
