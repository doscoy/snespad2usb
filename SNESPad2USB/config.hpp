#pragma once

#define MYAPP_CONFIG_INTERVAL_MIN 1
#define MYAPP_CONFIG_INTERVAL_DEFAULT 5
#define MYAPP_CONFIG_INTERVAL_MAX 100
#define MYAPP_CONFIG_CALL_MIN 100
#define MYAPP_CONFIG_CALL_DEFAULT 1000
#define MYAPP_CONFIG_CALL_MAX 10000

namespace SNESPad2USB {

struct Config {
  uint8_t usb_poll_interval;
  uint16_t menu_call;
  uint8_t current_setting;
};

}
