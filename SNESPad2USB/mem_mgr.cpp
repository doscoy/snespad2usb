#include "mem_mgr.hpp"

#include <Wire.h>

namespace SNESPad2USB {

MemoryMgr256k::MemoryMgr256k(const uint8_t address) : address_(address) {}

namespace {
void EPPROMAddressing(const uint16_t address) {
  Wire.write(address >> 8);
  Wire.write(address & 0x00FF);
}
}

void MemoryMgr256k::LoadConfig(Config* config) {
  Wire.beginTransmission(address_);
  EPPROMAddressing(kConfigRange.beg);
  Wire.endTransmission();
  Wire.requestFrom(address_, sizeof(*config));
  
  uint8_t* tmp{reinterpret_cast<uint8_t*>(config)};
  int counter{0};
  while (Wire.available()) {
    tmp[counter] = Wire.read();
    ++counter;
  }

  if (config->usb_poll_interval < MYAPP_CONFIG_INTERVAL_MIN ||
      config->usb_poll_interval > MYAPP_CONFIG_INTERVAL_MAX) {
    config->usb_poll_interval = MYAPP_CONFIG_INTERVAL_DEFAULT;
  }
  if (config->menu_call < MYAPP_CONFIG_CALL_MIN ||
      config->menu_call > MYAPP_CONFIG_CALL_MAX) {
    config->menu_call = MYAPP_CONFIG_CALL_DEFAULT;
  }
  if (config->current_setting > (kSettingIndexLen - 1)) {
    config->current_setting = 0;
  }
}

void MemoryMgr256k::SaveConfig(const Config& config) {
  Wire.beginTransmission(address_);
  EPPROMAddressing(kConfigRange.beg);
  Wire.write(reinterpret_cast<const uint8_t*>(&config), sizeof(config));
  Wire.endTransmission();
  delay(100);
}

void MemoryMgr256k::SaveCurrentSetting(const uint8_t current_setting) {
  Config tmp;
  LoadConfig(&tmp);
  tmp.current_setting = current_setting;
  SaveConfig(tmp);
}

void MemoryMgr256k::LoadSetting(ParameterSet* param_set, const uint8_t index) {
  if (index < kSettingIndexLen) {
    const uint16_t start{static_cast<uint16_t>(kSettingRange.beg + (sizeof(*param_set) * index))};
    uint8_t* tmp{reinterpret_cast<uint8_t*>(param_set)};
    int counter{0};
    for (uint16_t addr = start; addr < start + sizeof(*param_set); addr += 32) {
      Wire.beginTransmission(address_);
      EPPROMAddressing(addr);
      Wire.endTransmission();
      Wire.requestFrom(address_, 32);
      while (Wire.available()) {
        tmp[counter] = Wire.read();
        ++counter;
      }
    }
  }
}

void MemoryMgr256k::SaveSetting(const ParameterSet& param_set, const uint8_t index) {
  if (index < kSettingIndexLen) {
    const uint16_t start{static_cast<uint16_t>(kSettingRange.beg + (sizeof(param_set) * index))};
    const uint8_t* tmp{reinterpret_cast<const uint8_t*>(&param_set)};
    for (uint16_t addr = start; addr < start + sizeof(param_set); addr += 32) {
      Wire.beginTransmission(address_);
      EPPROMAddressing(addr);
      Wire.write(&tmp[addr - start], 32);
      Wire.endTransmission();
      delay(100);
    }
  }
}

void MemoryMgr256k::GetSettingName(char buffer[17], const uint8_t index) {
  if (index < kSettingIndexLen) {
    Wire.beginTransmission(address_);
    EPPROMAddressing(kSettingRange.beg + (sizeof(ParameterSet) * index));
    Wire.endTransmission();
    Wire.requestFrom(address_, 16);
    int counter{0};
    while (Wire.available()) {
      buffer[counter] = Wire.read();
      ++counter;
    }
    buffer[16] = 0;
  }
}

}
