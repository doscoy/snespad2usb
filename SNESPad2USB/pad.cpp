#include "pad.hpp"

namespace SNESPad2USB {

SNESPad::SNESPad(uint8_t data_pin, uint8_t load_pin, uint8_t clock_pin)
    : data_pin_(data_pin), load_pin_(load_pin), clock_pin_(clock_pin) {}

void SNESPad::Init() {
  pinMode(data_pin_, INPUT_PULLUP);
  pinMode(load_pin_, OUTPUT);
  pinMode(clock_pin_, OUTPUT);
}

void SNESPad::UpdateButton() {
  digitalPinToPort(load_pin_)->OUTSET.reg = digitalPinToBitMask(load_pin_);
  digitalPinToPort(load_pin_)->OUTCLR.reg = digitalPinToBitMask(load_pin_);
  for (uint8_t i = 0; i < SNESPAD_MAX; ++i) {
    const auto port{digitalPinToPort(data_pin_)->IN.reg};
    const auto mask{digitalPinToBitMask(data_pin_)};
    const bool tmp{(port & mask) == 0};
    buttons_[i].Update(tmp);
    digitalPinToPort(clock_pin_)->OUTSET.reg = digitalPinToBitMask(clock_pin_);
    digitalPinToPort(clock_pin_)->OUTCLR.reg = digitalPinToBitMask(clock_pin_);
  }
  if (buttons_[SNESPAD_SELECT].Release()) select_press_time_ = millis();
}

void SNESPad::UpdateReport() {
  keyboard_report_ = KeyboardReport{};
  gamepad_report_ = GamepadReport{};
  flip_flag_ = false;
  for (uint8_t i = 0; i < SNESPAD_MAX; ++i) {
    const Button& button{buttons_[i]};
    const ButtonParameter& param{setting_.params[i][0]};
    if ((param.inst.map.content == INST_FLIP) && button.Press()) {
      flip_flag_ = true;
      break;
    }
  }
  press_flag_ = false;
  uint8_t flip{flip_flag_ ? static_cast<uint8_t>(1) : static_cast<uint8_t>(0)};
  for (uint8_t i = 0; i < SNESPAD_MAX; ++i) {
    const Button& button{buttons_[i]};
    const ButtonParameter& param{setting_.params[i][flip]};
    if ((i != SNESPAD_SELECT) && button.Down()) press_flag_ = true;
    if (param.inst.map.content == INST_KEYBOARD) {
      WriteKeyboardReport(button, param, &keyboard_report_);
    } else if (param.inst.map.content == INST_GAMEPAD) {
      WriteGamePadReport(button, param, &gamepad_report_);
    }
  }
}

void SNESPad::SetDefaultParams() {
  strcpy(setting_.name, "default gamepad");
  setting_.name[16] = 0;
  setting_.params[SNESPAD_B][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_B][0].inst.map.param_len = 1;
  setting_.params[SNESPAD_B][0].item[0] = GAMEPAD_BUTTON1;
  setting_.params[SNESPAD_B][0].item[1] = 0;
  setting_.params[SNESPAD_B][0].item[2] = 0;
  setting_.params[SNESPAD_A][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_A][0].inst.map.param_len = 1;
  setting_.params[SNESPAD_A][0].item[0] = GAMEPAD_BUTTON2;
  setting_.params[SNESPAD_A][0].item[1] = 0;
  setting_.params[SNESPAD_A][0].item[2] = 0;
  setting_.params[SNESPAD_Y][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_Y][0].inst.map.param_len = 1;
  setting_.params[SNESPAD_Y][0].item[0] = GAMEPAD_BUTTON3;
  setting_.params[SNESPAD_Y][0].item[1] = 0;
  setting_.params[SNESPAD_Y][0].item[2] = 0;
  setting_.params[SNESPAD_X][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_X][0].inst.map.param_len = 1;
  setting_.params[SNESPAD_X][0].item[0] = GAMEPAD_BUTTON4;
  setting_.params[SNESPAD_X][0].item[1] = 0;
  setting_.params[SNESPAD_X][0].item[2] = 0;
  setting_.params[SNESPAD_L][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_L][0].inst.map.param_len = 1;
  setting_.params[SNESPAD_L][0].item[0] = GAMEPAD_BUTTON5;
  setting_.params[SNESPAD_L][0].item[1] = 0;
  setting_.params[SNESPAD_L][0].item[2] = 0;
  setting_.params[SNESPAD_R][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_R][0].inst.map.param_len = 1;
  setting_.params[SNESPAD_R][0].item[0] = GAMEPAD_BUTTON6;
  setting_.params[SNESPAD_R][0].item[1] = 0;
  setting_.params[SNESPAD_R][0].item[2] = 0;
  setting_.params[SNESPAD_SELECT][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_SELECT][0].inst.map.param_len = 1;
  setting_.params[SNESPAD_SELECT][0].item[0] = GAMEPAD_BUTTON7;
  setting_.params[SNESPAD_SELECT][0].item[1] = 0;
  setting_.params[SNESPAD_SELECT][0].item[2] = 0;
  setting_.params[SNESPAD_START][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_START][0].inst.map.param_len = 1;
  setting_.params[SNESPAD_START][0].item[0] = GAMEPAD_BUTTON8;
  setting_.params[SNESPAD_START][0].item[1] = 0;
  setting_.params[SNESPAD_START][0].item[2] = 0;
  setting_.params[SNESPAD_UP][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_UP][0].inst.map.param_len = 2;
  setting_.params[SNESPAD_UP][0].item[0] = GAMEPAD_AXISY;
  setting_.params[SNESPAD_UP][0].item[1] = 0;
  setting_.params[SNESPAD_UP][0].item[2] = 0;
  setting_.params[SNESPAD_DOWN][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_DOWN][0].inst.map.param_len = 2;
  setting_.params[SNESPAD_DOWN][0].item[0] = GAMEPAD_AXISY;
  setting_.params[SNESPAD_DOWN][0].item[1] = 255;
  setting_.params[SNESPAD_DOWN][0].item[2] = 0;
  setting_.params[SNESPAD_RIGHT][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_RIGHT][0].inst.map.param_len = 2;
  setting_.params[SNESPAD_RIGHT][0].item[0] = GAMEPAD_AXISX;
  setting_.params[SNESPAD_RIGHT][0].item[1] = 255;
  setting_.params[SNESPAD_RIGHT][0].item[2] = 0;
  setting_.params[SNESPAD_LEFT][0].inst.map.content = INST_GAMEPAD;
  setting_.params[SNESPAD_LEFT][0].inst.map.param_len = 2;
  setting_.params[SNESPAD_LEFT][0].item[0] = GAMEPAD_AXISX;
  setting_.params[SNESPAD_LEFT][0].item[1] = 0;
  setting_.params[SNESPAD_LEFT][0].item[2] = 0;
}
  
void SNESPad::WriteKeyboardReport(const Button& button, const ButtonParameter& param, KeyboardReport* report) {
  if (button.Release()) return;
  for (uint8_t i = 0; i < param.inst.map.param_len && report->count < sizeof(report->keycode); ++i) {
    report->keycode[report->count] = param.item[i];
    if (param.item[i] >= 0xE0 && param.item[i] <= 0xE7) {
      report->mod |= 1 << (param.item[i] & 0x0F);
    }
    report->count++;
  }
}
  
void SNESPad::WriteGamePadReport(const Button& button, const ButtonParameter& param, GamepadReport* report) {
  if (button.Release()) return;
  if (param.item[0] <= GAMEPAD_BUTTON16) {
    report->button |= (1 << param.item[0]);
  } else {
    switch (param.item[0]) {
      case GAMEPAD_AXISX:
        report->axis_x = param.item[1];
        break;
      case GAMEPAD_AXISY:
        report->axis_y = param.item[1];
        break;
      case GAMEPAD_AXISZ:
        report->axis_z = param.item[1];
        break;
      case GAMEPAD_ROTATIONZ:
        report->rot_z = param.item[1];
        break;
      default:
        break;
    }
  }
}

}
