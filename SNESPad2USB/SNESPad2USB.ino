#include <Wire.h>

#include "config.hpp"
#include "mem_mgr.hpp"
#include "pad.hpp"
#include "usb_mgr.hpp"
#include "gui.hpp"

constexpr char kDeviceName[]{"SNESPad2USB"};
constexpr byte kDataPin{3};
constexpr byte kLoadPin{2};
constexpr byte kClockPin{1};
constexpr byte kOLEDAddress{0x3C};
constexpr byte kEEPROMAddress{0x50};

SNESPad2USB::Config config;
SNESPad2USB::SNESPad pad{kDataPin, kLoadPin, kClockPin};
SNESPad2USB::USBReportMgr usb;
SNESPad2USB::MemoryMgr256k mem{kEEPROMAddress};
SNESPad2USB::GUI128x64 gui{&config, &pad, &usb, &mem};

void setup() {
  Wire.begin();
  
  mem.LoadConfig(&config);
  
  pad.Init();
  mem.LoadSetting(pad.GetParameterSet(), config.current_setting);
  
  usb.Init(config.usb_poll_interval, kDeviceName);
  
  gui.Init(kOLEDAddress);

  gui.PrintString("wait usb mounting");
  while(!usb.Mounted()) delay(100);
  gui.OffDisplay();
}

void loop() {
  pad.UpdateButton();
  pad.UpdateReport();

  if (gui.GetEnable()) {
    gui.Draw();
  } else {
    usb.RemoteWakeUp(pad.GetPressFlag());
    usb.SendGamepadReport(pad.GetGamepadReport());
    usb.SendKeyboardReport(pad.GetKeyboardReport());
    gui.SetEnable(pad.GetSelectPressTime() > config.menu_call);
  }
}
