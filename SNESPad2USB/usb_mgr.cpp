#include "usb_mgr.hpp"

namespace SNESPad2USB {

namespace {
constexpr uint8_t kGamepadRID{1};
constexpr uint8_t kKeyboardRID{2};
constexpr uint8_t kReportDescriptor[]{
  0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
  0x09, 0x05,                    // USAGE (Game Pad)
  0xa1, 0x01,                    // COLLECTION (Application)
  0x85, kGamepadRID,             //   REPORT_ID
  0x09, 0x01,                    //   USAGE (Pointer)
  0xa1, 0x00,                    //   COLLECTION (Physical)
  0x05, 0x09,                    //     USAGE_PAGE (Button)
  0x19, 0x01,                    //     USAGE_MINIMUM (Button 1)
  0x29, 0x10,                    //     USAGE_MAXIMUM (Button 16)
  0x15, 0x00,                    //     LOGICAL_MINIMUM (0)
  0x25, 0x01,                    //     LOGICAL_MAXIMUM (1)
  0x35, 0x00,                    //     PHYSICAL_MINIMUM (0)
  0x45, 0x01,                    //     PHYSICAL_MAXIMUM (1)
  0x75, 0x01,                    //     REPORT_SIZE (1)
  0x95, 0x10,                    //     REPORT_COUNT (16)
  0x81, 0x02,                    //     INPUT (Data,Var,Abs)
  0x05, 0x01,                    //     USAGE_PAGE (Generic Desktop)
  0x09, 0x30,                    //     USAGE (X)
  0x09, 0x31,                    //     USAGE (Y)
  0x09, 0x32,                    //     USAGE (Z)
  0x09, 0x35,                    //     USAGE (Rz)
  0x15, 0x00,                    //     LOGICAL_MINIMUM (0)
  0x26, 0xff, 0x00,              //     LOGICAL_MAXIMUM (255)
  0x35, 0x00,                    //     PHYSICAL_MINIMUM (0)
  0x46, 0xff, 0x00,              //     PHYSICAL_MAXIMUM (255)
  0x65, 0x00,                    //     UNIT (None)
  0x75, 0x08,                    //     REPORT_SIZE (8)
  0x95, 0x04,                    //     REPORT_COUNT (4)
  0x81, 0x02,                    //     INPUT (Data,Var,Abs)
  0xc0,                          //   END_COLLECTION
  0xc0,                          // END_COLLECTION
  TUD_HID_REPORT_DESC_KEYBOARD( HID_REPORT_ID(kKeyboardRID) )
};
}

void USBReportMgr::Init(const uint8_t interval_ms, const char* device_name) {
  interval_ms_ = interval_ms;
  freq_ = CalcFreq(interval_ms_);
  wait_micros_ = interval_ms_ * 1000;
  usb_hid_.setPollInterval(interval_ms_);
  usb_hid_.setReportDescriptor(kReportDescriptor, sizeof(kReportDescriptor));
  usb_hid_.setStringDescriptor(device_name);
  usb_hid_.begin();
}
 
void USBReportMgr::SendKeyboardReport(KeyboardReport& report) {
  while((micros() - last_send_time_) < wait_micros_) yield();
  usb_hid_.keyboardReport(kKeyboardRID, report.mod, report.keycode);
  last_send_time_ = micros();
}
  
void USBReportMgr::SendGamepadReport(GamepadReport& report) {
  while((micros() - last_send_time_) < wait_micros_) yield();
  usb_hid_.sendReport(kGamepadRID, &report, sizeof(report));
  last_send_time_ = micros();
}

}
