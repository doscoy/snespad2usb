#pragma once

#include <Arduino.h>
#include <Adafruit_TinyUSB.h>

namespace SNESPad2USB {

struct KeyboardReport {
  uint8_t keycode[6]{0, 0, 0, 0, 0, 0};
  uint8_t mod{0};
  uint8_t count{0};
};

struct GamepadReport {
  uint16_t button{0x00};
  uint8_t axis_x{0x80};
  uint8_t axis_y{0x80};
  uint8_t axis_z{0x80};
  uint8_t rot_z{0x80};
};

class USBReportMgr {
 public:
  void Init(const uint8_t, const char*);
  void SendKeyboardReport(KeyboardReport&);
  void SendGamepadReport(GamepadReport&);
  bool Mounted() { return USBDevice.mounted(); }
  bool Ready() { return usb_hid_.ready(); }
  void RemoteWakeUp(const bool flag) {
    if (USBDevice.suspended() && flag) {
      USBDevice.remoteWakeup();
      delay(500);
    }
  }
  static float CalcFreq(const uint8_t interval_ms) { return 1000.0f / (interval_ms * 2.0f); }
  
 private:
  Adafruit_USBD_HID usb_hid_;
  uint8_t interval_ms_;
  float freq_;
  unsigned long wait_micros_;
  unsigned long last_send_time_;
};

}
